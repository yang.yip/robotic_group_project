function imageplay(frame)

%close all; clc; clear;
% addpath(genpath('./utils'));
% Octree
% close all;

% doplot3 = @(p,varargin)scatter3(p(:,1),p(:,2),p(:,3),varargin{:});
binCapacity = 100;
% style = 'equal';
style = 'weighted';


fname = ['./Stationary_peoplewalkrun_2018-03-25-12-15-13/remove_ground/remove_ground_points_frame_' num2str(frame) '.pcd'];
disp(['Loading: ' fname]);
frame = frame-1;
ptCloud = pcread(fname);
ptCloud = filterPC(ptCloud);
xyzPoints = ptCloud.Location;

ot = OcTree(xyzPoints,'style',style,'binCapacity',binCapacity);
ot.shrink;
%ot.findroi;

% h = figure(108); ax = axes('Parent',h); set(h,'Color','white');
% title(['Octree Frame: ' num2str(frame)]);
% xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)');

boxH = ot.plot;
cols = lines(ot.BinCount);
for i = 1:ot.BinCount
    set(boxH(i),'Color',cols(i,:),'LineWidth', 2);
    %doplot3(xyzPoints(ot.PointBins==i,:),'.');
end
%axis([-4 1 -4 2 -1 0.5]);
% axis([-5 5 -5 5 -2 2]);
% view([0,0]);
%set(ax,'Color',[0 0 0],'DataAspectRatio',[1 1 1]);
%saveas(h,'H8.fig');

% Density and State Estimation

density_thresh = 20;

fname = ['./Stationary_peoplewalkrun_2018-03-25-12-15-13/remove_ground/remove_ground_points_frame_' num2str(frame) '.pcd'];
disp(['Loading: ' fname]);

ptCloudA = pcread(fname);
ptCloudA = filterPC(ptCloudA);

[bbox]=voxelMovement(ptCloudA,ptCloud,ot,density_thresh);
idx = find(bbox(:,1)~=0);

C = ones(size(ot.Points));
C(:,1) = .5;
C(:,2) = .5;
C(:,3) = .5;
reseq = [];
reseq(:,1) = ot.BinBoundaries(:,1);
reseq(:,2) = ot.BinBoundaries(:,4);
reseq(:,3) = ot.BinBoundaries(:,2);
reseq(:,4) = ot.BinBoundaries(:,5);
reseq(:,5) = ot.BinBoundaries(:,3);
reseq(:,6) = ot.BinBoundaries(:,6);

for i=1:length(idx)
    indices = findPointsInROI(ptCloud, reseq(idx(i),:));
    C(indices,1) = 1;
    C(indices,2) = 0;
    C(indices,3) = 0;
end
% h = figure(109); ax = axes('Parent',h); set(h,'Color','black');
% pcshow(xyzPoints,C,'MarkerSize',20);
% xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)');
% view([-90,90]);
% set(ax,'Color',[0 0 0],'DataAspectRatio',[1 1 1]);
%saveas(h,'H9.fig');


% Voxel Merging
%  h = figure(110); ax = axes('Parent',h); set(h,'Color','black');
for i=1:length(idx)
    
    binMinMax = ot.BinBoundaries(idx(i),:);
    pts = cat(1, binMinMax([...
        1 2 3; 4 2 3; 4 5 3; 1 5 3; 1 2 3;...
        1 2 6; 4 2 6; 4 5 6; 1 5 6; 1 2 6; 1 2 3]),...
        nan(1,3), binMinMax([4 2 3; 4 2 6]),...
        nan(1,3), binMinMax([4 5 3; 4 5 6]),...
        nan(1,3), binMinMax([1 5 3; 1 5 6]));
%     h = plot3(pts(:,1),pts(:,2),pts(:,3)); hold on;
%     set(h,'Color',cols(i,:),...
%         'LineWidth',2);
end
% pcshow(xyzPoints,C,'MarkerSize',20);
% xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)');
% axis([-5 5 -5 5 -2 2]);
% view([-90,90]);
% set(h,'Color','k');
% set(ax,'Color',[0 0 0],'DataAspectRatio',[1 1 1]);
%saveas(h,'H10.fig');

[ot,bbox]=voxelMerge(ot,bbox);
idx = find(bbox(:,1)~=0);

% h = figure(111); ax = axes('Parent',h); set(h,'Color','black');
for i=1:length(idx)
    
    binMinMax = ot.BinBoundaries(idx(i),:);
    pts = cat(1, binMinMax([...
        1 2 3; 4 2 3; 4 5 3; 1 5 3; 1 2 3;...
        1 2 6; 4 2 6; 4 5 6; 1 5 6; 1 2 6; 1 2 3]),...
        nan(1,3), binMinMax([4 2 3; 4 2 6]),...
        nan(1,3), binMinMax([4 5 3; 4 5 6]),...
        nan(1,3), binMinMax([1 5 3; 1 5 6]));
%     h = plot3(pts(:,1),pts(:,2),pts(:,3)); hold on;
%     set(h,'Color',cols(i,:),...
%         'LineWidth',2);
end
% pcshow(xyzPoints,C,'MarkerSize',20);
% xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)');
% axis([-5 5 -5 5 -2 2]);
% view([-90,90]);
% set(ax,'Color',[0 0 0],'DataAspectRatio',[1 1 1]);
%saveas(h,'H11.fig');

% Classification

C = ones(size(ot.Points));
C(:,1) = .5;
C(:,2) = .5;
C(:,3) = .5;

[ot,bbox,target]=voxelClassification(ot,bbox);
hold off;
x = figure(112); ax = axes('Parent',x); set(x,'Color','black');
for i=1:length(target)
    if ~isempty(target(i).roi)
        indices = findPointsInROI(ptCloud,target(i).roi );
        
        C(indices,1) = target(i).color(1);
        C(indices,2) = target(i).color(2);
        C(indices,3) = target(i).color(3);
        
        binMinMax = ot.BinBoundaries(target(i).index,:);
        pts = cat(1, binMinMax([...
            1 2 3; 4 2 3; 4 5 3; 1 5 3; 1 2 3;...
            1 2 6; 4 2 6; 4 5 6; 1 5 6; 1 2 6; 1 2 3]),...
            nan(1,3), binMinMax([4 2 3; 4 2 6]),...
            nan(1,3), binMinMax([4 5 3; 4 5 6]),...
            nan(1,3), binMinMax([1 5 3; 1 5 6]));
        x = plot3(pts(:,1),pts(:,2),pts(:,3)); hold on;
        set(x,'Color',cols(i,:),...
            'LineWidth',2);
    end
end
hold off
pcshow(xyzPoints,C,'MarkerSize',20);

xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)');
axis([-15 15 -10 10 -2 2]);
view([-90,90]);
set(ax,'Color',[0 0 0],'DataAspectRatio',[1 1 1]);
hold off
%saveas(h,'H12.fig');