figure(100)

hold on
for i_frame = 1:ii-500
    str = num2str(i_frame);
    for j_point = 1:size(trackpoint{i_frame},1)
        pos = trackpoint{i_frame}(j_point,:);
        
        plot3(pos(1),pos(2),pos(3),'x')
    end
end


n_tracks = numel(track);
colors = hsv(n_tracks);

all_points = vertcat(trackpoint{:});
for i_track = 1:n_tracks
    track = adjacency_tracks{i_track};
    track_points = all_points(track, :);
    plot3(track_points(:,1),track_points(:,2),track_points(:,3),'Color',colors(i_track,:));
end