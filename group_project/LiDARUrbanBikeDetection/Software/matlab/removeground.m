% folder = uigetdir('.','Select PCD Folder');


for i = 100:800
    fname = ['./Stationary_peoplewalkrun_2018-03-25-12-15-13/pcd_merged/merged_velodyne_points_frame_' num2str(i) '.pcd'];
    disp(['loading: ' fname]);
    ptcloud = pcread(fname);
    ptcloud = filterPC(ptcloud);
    ma = 0.1;
    refvec = [0,0,1];
    [model1,inlier,outlier] = pcfitplane(ptcloud,ma,refvec);
    remain = select(ptcloud,outlier);
%     xlim = [0,1]; ylim = [0,1]; zlim = [0,1];
%     player = pcplayer(xlim,ylim,zlim);
%     [xlim,ylim,zlim] = findLim(remain,xlim,ylim,zlim);
%     player.Axes.XLim = [-15,15];
%     player.Axes.YLim = [-15,15];
    
    figure(1)
    pcshow(remain);
%     outdir = fullfile(parent,'pcd_remove');
%     if ~isdir(outdir)
%         mkdir(outdir);
%     end
    pcwrite(remain,['remove_ground_points_frame_' num2str(i) '.pcd'],'Encoding','compressed');
end
